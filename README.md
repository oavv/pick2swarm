# pick2swarm

  

Earthworm PICK messages to Swarm CSV file format for displaying tags on helicorder.

  
  

## Installation

Clone Earthworm from source:

    git clone https://gitlab.com/seismic-software/earthworm.git

  

cd to src/data_exchange/ folder.

    cd earthworm_git/src/data_exchange/

  

Now clone pick2swarm source files:

    git clone https://gitlab.com/oavv/pick2swarm.git

  
Edit **data_exchange/Makefile** to add the new *pick2swarm* folder to the compile chain:

    nano Makefile

Find the **ALL_MODULES = \\** entry and add:

    pick2swarm \

Then find the **UNIX_MODULES = \\** entry and add:

    pick2swarm \

Save changes to **Makefile** Go back to Earthworm source folder:

    cd ..

Export variables:

    export EW_INSTALL_VERSION=earthworm_git

Source Earthworm environment variables for compiling:

    source ../environment/ew_linux.bash

  

Compile:

    make unix

  
  

Watch for errors during compilation

  

## Usage

Add *pick2swarm* to your Earthworm *startstop* files. Don't forget to define a new module ID for **MOD_PICK2SWARM** in your *earthworm.d* file.

If program is running successfully, you will get CSV files like this:

    2022-11-09 17:16:53,PEH HHZ VV CP,DOWN_2
    2022-11-09 17:16:57,PEH HHZ VV CP,UNKNOWN_2
    2022-11-09 17:16:53,PEH HHN VV CP,DOWN_2
    2022-11-09 17:16:57,PEH HHN VV CP,UNKNOWN_1
    2022-11-09 17:16:57,PEH HHE VV CP,UNKNOWN_1
    2022-11-09 17:17:12,MAQ HHZ VV CP,UNKNOWN_0
    2022-11-09 17:17:11,MAQ HHE VV CP,UNKNOWN_0
    2022-11-09 17:17:11,MAQ HHE VV CP,UNKNOWN_0
    2022-11-09 17:17:11,MAQ HHN VV CP,UNKNOWN_2
    2022-11-09 17:17:14,MAQ HHE VV CP,DOWN_4
    2022-11-09 17:17:16,MAQ HHE VV CP,UNKNOWN_0
    2022-11-09 17:17:24,FRO HHZ VV CP,UNKNOWN_1

Please note Swarm does not support millisecond precision on it's tag file, so you will lose that information.


## Support

I actively check earthworm google groups:
https://groups.google.com/g/earthworm_forum

  
  

## Contributing

Feel free to contribute or suggest any modifications.

  

## Authors and acknowledgment

Coded by Victor Preatoni for OAVV-SEGEMAR (Observatorio Argentino de Vigilancia Volcanica | Servicio Geologico Minero Argentino).
Based on Pete Lombard's *ew2file* module.
