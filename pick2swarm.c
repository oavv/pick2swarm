/*
 *   $Id: pick2swarm.c
 *
 *    Revision history:
 *
 *     Revision 1.0  2022/11/03 victor preatoni
 *     Initial revision. Based on ew2file
 *
 */

/*
 *   pick2swarm.c
 *
 *   Program to read TYPE_PICK_SCNL messages from a ring
 *   and write them to a CSV using Swarm tagging format:
 *
 * YYYY-mm-dd HH:mm:ss      SSSS CCC NN LL      tag
 *
 * Where:
 *      YYYY    4 digit year
 *      mm      2 digit month
 *      dd      2 digit day
 *      HH      2 digit 24-hour
 *      MM      2 digit minute
 *      ss      2 digit seconds
 *      SSSS    Station
 *      CCC     Channel
 *      NN      Network
 *      LL      Location
 *      tag     Pick direction and quality tag
 *
 * Eg: 2022-04-07 11:13:02,BARZ HHZ TC PP,UP_3
 *
 *   Victor Preatoni | OAVV-SEGEMAR | 2022-11-03
 *
 */

#define PICK2SWARM_VERSION "1.0.2 - Aug 10, 2022"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h>
#include <time.h>
#include <time_ew.h>
#include <errno.h>
#include <signal.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>
#include <mem_circ_queue.h>
#include <read_arc.h>

#ifdef _LINUX
#include <sys/param.h> 	/* needed for MAXPATHLEN */
#endif

#ifdef _MACOSX
#include <sys/param.h> 	/* needed for MAXPATHLEN */
#endif

#ifndef MAXPATHLEN
#define MAXPATHLEN 256
#endif

#define MAX_STR 512


/* Functions in this source file
*******************************/
typedef struct {
        char dateStr[11];
        char timeStr[9];
        char s[6];
        char c[4];
        char n[3];
        char l[3];
        char tag[32];
} PickLine;


void pick2swarm_config(char *);
void pick2swarm_lookup(void);
void pick2swarm_status(MSG_LOGO *, char *);
int writeFile(char *msg, size_t msglen, char *dir, char *filename, char *errText);
int buildPickString(PickLine pick, char *buffer);
int parsePick(char *line, PickLine *pick);



/* Thread things
***************/
#define THREAD_STACK 8192
static ew_thread_t tidDup;              /* Dup thread id */
static ew_thread_t tidStacker;          /* Thread moving messages from transport */
                                     /*   to queue */

#define MSGSTK_OFF    0              /* MessageStacker has not been started      */
#define MSGSTK_ALIVE  1              /* MessageStacker alive and well            */
#define MSGSTK_ERR   -1              /* MessageStacker encountered error quit    */
volatile int MessageStackerStatus = MSGSTK_OFF;

QUEUE OutQueue; 		     /* from queue.h, queue.c; sets up linked    */
                                     /*    list via malloc and free              */
thr_ret WriteThd( void * );
thr_ret MessageStacker( void * );    /* used to pass messages between main thread */
                                     /*   and Dup thread */
/* Message Buffers to be allocated
*********************************/
static char *Rdmsg = NULL;           /* msg retrieved from transport      */
static char *Wrmsg = NULL;           /*  message to write to file   */

/* Timers
******/
time_t MyLastBeat;            /* time of last local (into Earthworm) hearbeat */

char *cmdname;          /* pointer to executable name */
pid_t MyPid;		/* Our own pid, sent with heartbeat for restart purposes */

#define DEF_HEARTFILE_MSG "Alive\n"
#define DEF_HEARTFILE_SFX "hrt"

#define MAX_SERIAL     10000000
#define HEART_SERIAL    9999999
#define SERIAL_FORMAT  "%07d"

/* Things to read or derive from configuration file
**************************************************/
char      InRing[MAX_RING_STR];    /* name of transport ring for input  */
char      MyModName[MAX_MOD_STR];  /* speak as this module name/id      */
int       LogSwitch;        /* 0 if no logfile should be written        */
int       DebugSwitch;      /* 1 to log some debug info                 */
int       HeartBeatInt;     /* seconds between heartbeat messages       */
long      MaxMsgSize;       /* max size for input/output msgs           */
int       QueueSize;	    /* max messages in output circular buffer   */
SHM_INFO  InRegion;         /* shared memory region to use for input    */
char      *CSVDir;          /* destination directories                  */
char      *CSVSuffix;       /* CSV file extension                       */
char      *CSVPrefix;       /* CSV file prefix                          */
char      *CSVseparator;    /* CSV field separator                      */


/* Things to look up in the earthworm.h tables with getutil.c functions
**********************************************************************/
static long          InRingKey;     /* key of transport ring for input    */
static MSG_LOGO HeartLogo;          /* logo of heartbeat message          */
static MSG_LOGO ErrorLogo;          /* logo of error message              */
#define MAXLOGO   3
static MSG_LOGO GetLogo[MAXLOGO];   /* array for requesting module,type,instid */
static short 	  nLogo;            /* number of logos to get                   */
static unsigned char TypeError;
static unsigned char TypePickSCNL;

/* Error messages used by pick2swarm
***********************************/
#define  ERR_MISSMSG       0   /* message missed in transport ring        */
#define  ERR_TOOBIG        1   /* retreived msg too large for buffer      */
#define  ERR_NOTRACK       2   /* msg retreived; tracking limit exceeded  */
#define  ERR_QUEUE         3   /* queue error                             */
#define  ERR_WRITE         4   /* error writing message file              */

int main( int argc, char **argv )
{
    /* Other variables: */
    long     recsize;	/* size of retrieved message             */
    MSG_LOGO reclogo;	/* logo of retrieved message             */
    time_t   now;	/* current time, used for timing heartbeats */
    char     errText[256];     /* string for log/error/heartbeat messages           */

    /* Check command line arguments
     ******************************/
    cmdname = argv[0];
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <configfile>\n", cmdname);
        fprintf(stderr, "Version: %s\n", PICK2SWARM_VERSION);
        return(0);
    }

    /* Initialize name of log-file & open it
     ****************************************/
    logit_init(argv[1], 0, 512, 1);

    /* Read the configuration file(s)
     ********************************/
    pick2swarm_config(argv[1]);
    logit("et", "%s(%s): Read command file <%s>\n", cmdname, MyModName, argv[1]);

    /* Look up important info from earthworm.h tables
     *************************************************/
    pick2swarm_lookup();

    /* Reinitialize the logging level
     *********************************/
    logit_init(argv[1], 0, 512, LogSwitch);

    /* Get our own Pid for restart purposes
     ***************************************/
    MyPid = getpid();
    if(MyPid == -1) {
        logit("e", "%s(%s): Cannot get pid; exiting!\n", cmdname, MyModName);
        return(0);
    }

    /* Allocate space for input/output messages for all threads
     ***********************************************************/
    /* Buffer for Read thread: */
    if ((Rdmsg = (char *)malloc(MaxMsgSize+1)) ==  NULL) {
        logit("e", "%s(%s): error allocating Rdmsg; exiting!\n", cmdname, MyModName);
        return(-1);
    }

    /* Buffers for Write thread: */
    if ((Wrmsg = (char *)malloc(MaxMsgSize+1)) ==  NULL) {
        logit("e", "%s(%s): error allocating Wrmsg; exiting!\n", cmdname, MyModName);
        return(-1);
    }

    /* Create a Mutex to control access to queue
     ********************************************/
    CreateMutex_ew();

    /* Initialize the message queue
     *******************************/
    initqueue(&OutQueue, (unsigned long)QueueSize, (unsigned long)(MaxMsgSize+1));

    /* Attach to Input/Output shared memory ring
     ********************************************/
    tport_attach(&InRegion, InRingKey);

    /* step over all messages from transport ring
     *********************************************/
    while (tport_getmsg(&InRegion, GetLogo, nLogo, &reclogo, &recsize, Rdmsg, MaxMsgSize) != GET_NONE);


    /* One heartbeat to announce ourselves to statmgr
     ************************************************/
    time(&MyLastBeat);
    sprintf(errText, "%ld %ld\n", (long)MyLastBeat, (long)MyPid);
    pick2swarm_status(&HeartLogo, errText);

    /* Start the message stacking thread if it isn't already running.
     ****************************************************************/
    if (MessageStackerStatus != MSGSTK_ALIVE) {
        if (StartThread(MessageStacker, (unsigned)THREAD_STACK, &tidStacker) == -1) {
            logit("e", "%s(%s): Error starting  MessageStacker thread; exiting!\n", cmdname, MyModName);
            tport_detach(&InRegion);
            exit(-1);
        }
        MessageStackerStatus = MSGSTK_ALIVE;
    }

    /* Start the file writing thread
     ***********************************/
    if (StartThread(WriteThd, (unsigned)THREAD_STACK, &tidDup) == -1) {
        logit("e", "%s(%s): Error starting Write thread; exiting!\n", cmdname, MyModName);
        tport_detach(&InRegion);
        exit(-1);
    }

    /* Start main pick2swarm service loop, which aimlessly beats its heart.
     **********************************/
    while (tport_getflag(&InRegion) != TERMINATE  && tport_getflag(&InRegion) != MyPid) {
        /* Beat the heart into the transport ring
            ****************************************/
        time(&now);
        if (difftime(now, MyLastBeat) > (double)HeartBeatInt) {
            sprintf(errText, "%ld %ld\n", (long)now, (long)MyPid);
            pick2swarm_status(&HeartLogo, errText);
            MyLastBeat = now;
        }

        /* take a brief nap; added 970624:ldd
            ************************************/
        sleep_ew(500);
    } /*end while of monitoring loop */

    /* Shut it down
     ***************/
    tport_detach( &InRegion );
    logit("t", "%s(%s): termination requested; exiting!\n", cmdname, MyModName);
    exit(0);
}
/* *******************  end of main *******************************
******************************************************************/



/**************************  Main Write Thread   ***********************
 *          Pull a messsage from the queue, and write it to a FILE!  *
 **********************************************************************/
thr_ret WriteThd(void *dummy )
{
	MSG_LOGO reclogo;
	int ret;
	int i;
	long msgSize;
	char errText[256];
	char filename[64];
    char pickLine[64];
    PickLine pickStruct;
    
    
    while (1) {   /* main loop */
        /* Get message from queue
            *************************/
        RequestMutex();
        ret = dequeue(&OutQueue, Wrmsg, &msgSize, &reclogo);
        ReleaseMutex_ew();

        if (ret < 0) { /* -1 means empty queue */
            sleep_ew(500);
            continue;
        }

        /* Determine which GetLogo this message used */
        for (i = 0; i < nLogo; i++) {
            if ((GetLogo[i].type == WILD || GetLogo[i].type == reclogo.type) && (GetLogo[i].mod == WILD || GetLogo[i].mod == reclogo.mod) && (GetLogo[i].instid == WILD || GetLogo[i].instid == reclogo.instid))
            break;
        }
        
        if (i == nLogo) {
            logit("et", "%s error: logo <%d.%d.%d> not found\n", cmdname, reclogo.instid, reclogo.mod, reclogo.type);
            continue;
        }
        
        
        /* Generate search string */
        char startPickLine[5];
        sprintf(startPickLine, "%u ", TypePickSCNL);
        
        /* Is message a TYPE_PICK_SCNL? */
        if (strncmp(Wrmsg, startPickLine, strlen(startPickLine)) == 0) { /* Found PICK_SCNL message */
            if (DebugSwitch) { /* If debug enabled ... */
                logit("o" ,"Got line: %s", Wrmsg);
            }
            
            if (parsePick(Wrmsg, &pickStruct) /*== 7*/) { /* Got all parameters, valid line */
                if (DebugSwitch) { /* If debug enabled ... */
                    logit("o" ,"Parse status:\n");
                    logit("o" ,"S: %s\nC: %s\nN: %s\nL: %s\n", pickStruct.s, pickStruct.c, pickStruct.n, pickStruct.l);
                    logit("o" ,"Date: %s\nTime: %s\nTag: %s\n", pickStruct.dateStr, pickStruct.timeStr, pickStruct.tag);
                }
                
                snprintf(filename, sizeof(filename), "%s_%s_%s.%s", CSVPrefix, pickStruct.l, pickStruct.dateStr, CSVSuffix);
                
                /* Write (or append) the line to file */
                buildPickString(pickStruct, pickLine);
                if (DebugSwitch) { /* If debug enabled ... */
                    logit("o" ,"Parsed as: %s", pickLine);
                    logit("o" ,"Appending to file: %s\n\n", filename);
                }
                if (writeFile(pickLine, strlen(pickLine), CSVDir, filename, errText) < 0) {
                    pick2swarm_status(&ErrorLogo, errText);
                }
            } //parsed line end
        } //valid line end     
        
    }   /* End of main loop */
}



/********************** Message Stacking Thread *******************
 *           Move messages from transport to memory queue         *
 ******************************************************************/
thr_ret MessageStacker(void *dummy )
{
    long         recsize;	/* size of retrieved message             */
    MSG_LOGO     reclogo;       /* logo of retrieved message             */
    time_t       now;
    char         errText[256]; /* string for log/error/heartbeat messages */
    int          ret;
    int          NumOfTimesQueueLapped= 0; /* number of messages lost due to queue lap */

    /* Tell the main thread we're ok
     ********************************/
    MessageStackerStatus = MSGSTK_ALIVE;

    /* Start main service loop for current connection
     ************************************************/
    while (1) {
        /* Get a message from transport ring
            ************************************/
        ret = tport_getmsg(&InRegion, GetLogo, nLogo, &reclogo, &recsize, Rdmsg, MaxMsgSize);

        switch (ret) {
        case GET_NONE:
            /* Wait if no messages for us */
            sleep_ew(100);
            continue;
            break;
        case GET_TOOBIG:
            time(&now);
            sprintf(errText, "%ld %hd msg[%ld] i%d m%d t%d too long for target", (long)now, (short)ERR_TOOBIG, recsize, (int) reclogo.instid, (int) reclogo.mod, (int)reclogo.type);
            pick2swarm_status(&ErrorLogo, errText);
            continue;
            break;
        case GET_MISS:
            time(&now);
            sprintf(errText, "%ld %hd missed msg(s) i%d m%d t%d in %s", (long)now, (short)ERR_MISSMSG, (int) reclogo.instid, (int) reclogo.mod, (int)reclogo.type, InRing);
            pick2swarm_status(&ErrorLogo, errText);
            break;
        case GET_NOTRACK:
            time(&now);
            sprintf( errText, "%ld %hd no tracking for logo i%d m%d t%d in %s", (long)now, (short)ERR_NOTRACK, (int) reclogo.instid, (int) reclogo.mod, (int)reclogo.type, InRing);
            pick2swarm_status(&ErrorLogo, errText);
            break;
        }

        /* Process retrieved msg (ret==GET_OK,GET_MISS,GET_NOTRACK)
        ***********************************************************/
        Rdmsg[recsize] = '\0';

        /* put it into the 'to be shipped' queue */
        /* the Write thread is in the biz of de-queueng and writing */
        RequestMutex();
        ret = enqueue(&OutQueue, Rdmsg, recsize, reclogo);
        ReleaseMutex_ew();

        switch(ret) {
        case -2:
            /* Serious: quit */
            /* Currently, eneueue() in mem_circ_queue.c never returns this error. */
            time(&now);
            sprintf(errText, "%ld %hd internal queue error. Terminating.", (long)now, (short)ERR_QUEUE);
            pick2swarm_status(&ErrorLogo, errText);
            goto error;
            break;
        case -1:
            time(&now);
            sprintf(errText,"%ld %hd message too big for queue.", (long)now, (short)ERR_QUEUE);
            pick2swarm_status(&ErrorLogo, errText);
            continue;
            break;
        case -3:
            NumOfTimesQueueLapped++;
            if (!(NumOfTimesQueueLapped % 5)) {
                logit("t", "%s(%s): Circular queue lapped 5 times. Messages lost.\n", cmdname, MyModName);
                if (!(NumOfTimesQueueLapped % 100)) {
                    logit("et", "%s(%s): Circular queue lapped 100 times. Messages lost.\n", cmdname, MyModName);
                }
            }
            continue;
        }
    } /* end of while */

    /* we're quitting
     *****************/
error:
    MessageStackerStatus = MSGSTK_ERR; /* file a complaint to the main thread */
    KillSelfThread(); /* main thread will not restart us */
    return THR_NULL_RET; /* Should never get here */
}

/*****************************************************************************
 *  pick2swarm_config() processes command file(s) using kom.c functions;        *
 *                    exits if any errors are encountered.	             *
 *****************************************************************************/
#define NCOMMANDS 11
void pick2swarm_config( char *configfile )
{
    int      ncommand;          /* # of required commands you expect to process   */
    char     init[NCOMMANDS];   /* init flags, one byte for each required command */
    int      nmiss;             /* number of required commands that were missed   */
    char    *com;
    int      nfiles;
    int      success;
    int      i;
    char*    str;

    /* Set to zero one init flag for each required command
     *****************************************************/
    ncommand = NCOMMANDS;
    
    for (i=0; i < ncommand; i++)
        init[i] = 0;
    
    CSVDir = NULL;
    CSVseparator = NULL;
    CSVSuffix = NULL;
    CSVPrefix = NULL;
    nLogo = 0;

    /* Open the main configuration file
     **********************************/
    nfiles = k_open(configfile);
    if (nfiles == 0) {
        logit("e" ,"%s: Error opening command file <%s>; exiting!\n", cmdname, configfile);
        exit(-1);
    }

    /* Process all command files
     ***************************/
    while(nfiles > 0) {   /* While there are command files open */
        while(k_rd()) {       /* Read next line from active file  */
            com = k_str();         /* Get the first token from line */

            /* Ignore blank lines & comments
                *******************************/
                if(!com)
                    continue;
                if(com[0] == '#')
                    continue;

            /* Open a nested configuration file
                **********************************/
                if(com[0] == '@') {
                    success = nfiles+1;
                    nfiles = k_open(&com[1]);
                    if (nfiles != success) {
                        logit("e" , "%s: Error opening command file <%s>; exiting!\n", cmdname, &com[1]);
                        exit(-1);
                    }
                    continue;
                }

            /* Process anything else as a command
                ************************************/
    /*0*/       if (k_its("LogFile")) {
                    LogSwitch = k_int();
                    init[0] = 1;
                }
                
    /*Optional*/else if (k_its("Debug")) {
                    DebugSwitch = k_int();
                }
                
    /*1*/       else if (k_its("MyModuleId")) {
                    str = k_str();
                    if(str)
                        strcpy( MyModName, str );
                    init[1] = 1;
                }
    /*2*/        else if (k_its("InRing")) {
                    str = k_str();
                    if (str)
                        strcpy( InRing, str );
                    init[2] = 1;
                }
    /*3*/       else if (k_its("HeartBeatInt")) {
                    HeartBeatInt = k_int();
                    init[3] = 1;
                }

            /* Enter installation & module & message types to get
                ****************************************************/
    /*4*/     else if( k_its("GetMsgLogo") ) {
                    if (nLogo >= MAXLOGO) {
                        logit( "e", "%s: Too many <GetMsgLogo> commands in <%s>", cmdname, configfile );
                        logit( "e", "; max=%d; exiting!\n", (int) MAXLOGO );
                        exit( -1 );
                    }
                    if ((str=k_str() ) != NULL) {
                        if (GetInst( str, &GetLogo[nLogo].instid ) != 0) {
                            logit("e" , "%s: Invalid installation name <%s>", cmdname, str);
                            logit("e" , " in <GetMsgLogo> cmd; exiting!\n");
                            exit(-1);
                        }
                    }
                    if ((str=k_str()) != NULL) {
                        if( GetModId( str, &GetLogo[nLogo].mod ) != 0) {
                            logit("e" , "%s: Invalid module name <%s>", cmdname, str);
                            logit("e" , " in <GetMsgLogo> cmd; exiting!\n");
                            exit(-1);
                        }
                    }
                    if ((str=k_str()) != NULL) {
                        if( GetType( str, &GetLogo[nLogo].type ) != 0 ) {
                            logit("e" , "%s: Invalid msgtype <%s>", cmdname, str);
                            logit("e" , " in <GetMsgLogo> cmd; exiting!\n");
                            exit(-1);
                        }
                    }
                    nLogo++;
                    init[4] = 1;
                }

                /* Maximum size (bytes) for incoming/outgoing messages
                    *****************************************************/
        /*5*/   else if (k_its("MaxMsgSize")) {
                        MaxMsgSize = k_long();
                        init[5] = 1;
                    }

                /* Maximum number of messages in outgoing circular buffer
                    ********************************************************/
        /*6*/     else if (k_its("QueueSize")) {
                        QueueSize = k_long();
                        init[6] = 1;
                    }

                /* CSV directory, for creating and writing files
                    ********************************************************/
        /*7*/     else if (k_its("CSVDir")) {
                        if ((str=k_str()) != NULL) {
                            if ((CSVDir = strdup(str)) == NULL) {
                                logit("e", "%s: out of memory for CSVDir\n", cmdname);
                                exit( -1 );
                            }
                        }
                        init[7] = 1;
                    }

                    /* CSV separator
                    ********************************************************/
        /*8*/     else if (k_its("CSVseparator")) {
                        if ((str=k_str()) != NULL) {
                            if ((CSVseparator = strdup(str)) == NULL) {
                                logit("e", "%s: out of memory for CSVseparator\n", cmdname);
                                exit(-1);
                            }
                        }
                        init[8] = 1;
                    }

                    /* CSV suffix
                    ********************************************************/
        /*9*/     else if (k_its("CSVSuffix")) {
                        if ((str=k_str()) != NULL) {
                            if ((CSVSuffix = strdup(str)) == NULL) {
                                logit("e", "%s: out of memory for CSVSuffix\n", cmdname);
                                exit(-1);
                            }
                        }
                        init[9] = 1;
                    }

                    /* CSV prefix
                    ********************************************************/
        /*10*/     else if (k_its("CSVPrefix")) {
                        if ((str=k_str()) != NULL) {
                            if ((CSVPrefix = strdup(str)) == NULL) {
                                logit("e" ,"%s: out of memory for CSVPrefix\n", cmdname);
                                exit(-1);
                            }
                        }
                        init[10] = 1;
                    }

                /* Unknown command
                    *****************/
                else {
                        logit("e", "%s: <%s> Unknown command in <%s>.\n", cmdname, com, configfile);
                        continue;
                }

                /* See if there were any errors processing the command
                *****************************************************/
                if (k_err()) {
                    logit("e", "%s: Bad <%s> command  in <%s>; exiting!\n", cmdname, com, configfile);
                    exit(-1);
                }
            }
            nfiles = k_close();
        }

    /* After all files are closed, check init flags for missed commands
     ******************************************************************/
    nmiss = 0;
    for (i = 0; i < ncommand; i++) {
        if(!init[i])
            nmiss++;
    }
    if (nmiss) {
        logit("e", "%s: ERROR, no ", cmdname);
        if (!init[0])
            logit("e", "<LogFile> ");
        if (!init[1])
            logit("e", "<MyModuleId> ");
        if (!init[2])
            logit("e", "<InRing> ");
        if (!init[3])
            logit("e", "<HeartBeatInt> ");
        if (!init[4])
            logit("e", "<GetMsgLogo> ");
        if (!init[5])
            logit("e", "<MaxMsgSize> ");
        if (!init[6])
            logit("e", "<Queue>");
        if (!init[7])
            logit("e", "<CSVDir>");
        if (!init[8])
            logit("e", "<CSVseparator>");
        if (!init[9])
            logit("e", "<CSVSuffix>");
        if (!init[10])
            logit("e", "<CSVPrefix>");
        
        logit("e", "command(s) in <%s>; exiting!\n", configfile);
        exit(-1);
    }

    /* Make sure our directories exist */
    if (RecursiveCreateDir(CSVDir) != EW_SUCCESS) {
        logit("e", "%s error creating CSVDir: %s\n", cmdname, strerror(errno));
        exit(-1);
    }

    return;
}

/****************************************************************************
 *  pick2swarm_lookup( )   Look up important info from earthworm.h tables      *
 ****************************************************************************/
void pick2swarm_lookup(void)
{
    /* Look up keys to shared memory regions
     *************************************/
    if ((InRingKey = GetKey(InRing)) == -1 ) {
        logit("e", "%s:  Invalid ring name <%s>; exiting!\n", cmdname, InRing);
        exit(-1);
    }

    /* Look up installations of interest
     *********************************/
    if (GetLocalInst( &HeartLogo.instid ) != 0) {
        logit("e", "%s: error getting local installation id; exiting!\n", cmdname);
        exit(-1);
    }
    ErrorLogo.instid = HeartLogo.instid;

    /* Look up modules of interest
     ***************************/
    if (GetModId(MyModName, &HeartLogo.mod) != 0) {
        logit("e", "%s: Invalid module name <%s>; exiting!\n", cmdname, MyModName);
        exit(-1);
    }
    ErrorLogo.mod = HeartLogo.mod;

    /* Look up message types of interest
     *********************************/
    if (GetType("TYPE_HEARTBEAT", &HeartLogo.type ) != 0) {
        logit("e", "%s: Invalid message type <TYPE_HEARTBEAT>; exiting!\n", cmdname);
        exit(-1);
    }
    if (GetType("TYPE_ERROR", &ErrorLogo.type ) != 0) {
        logit("e", "%s: Invalid message type <TYPE_ERROR>; exiting!\n", cmdname);
        exit(-1);
    }
    TypeError = ErrorLogo.type;

    if (GetType("TYPE_PICK_SCNL", &TypePickSCNL ) != 0) {
        logit("e", "%s: Invalid message type <TYPE_PICK_SCNL>; exiting!\n", cmdname);
        exit(-1);
    }

    return;
}

/***************************************************************************
 * pick2swarm_status() sends an error or heartbeat message to transport    *
 *    If the message is of TYPE_ERROR, the text will also be logged.       *
 *    Since pick2swarm_status is called be more than one thread, the logo  *
 *    and message must be constructed by the caller.                       *
 ***************************************************************************/
void pick2swarm_status(MSG_LOGO *logo, char *msg)
{
    size_t size;

    if (logo->type == TypeError)
        logit("et", "%s\n", msg);

    size = strlen(msg);   /* don't include the null byte in the message */

    /* Write the message to shared memory
     ************************************/
    if (tport_putmsg(&InRegion, logo, (long)size, msg) != PUT_OK) {
        logit("et", "%s(%s):  Error sending message to transport.\n", cmdname, MyModName);
    }
    
    return;
}


/* the function to write string to a named file
 * returns 0 upon success or -1 and sets errText upon failure
*/
int writeFile(char *msg, size_t msglen, char *dir, char *filename, char *errText)
{
    char outFile[MAXPATHLEN];
    time_t now;
    int ret;
    FILE *fp;
    char err[64];

    sprintf(outFile,  "%s/%s", dir, filename);
    
    fp = fopen(outFile, "ab");
    if (fp == NULL) {
        strcpy(err, "error creating file");
        goto error;
    }
    
    ret = (int)fwrite(msg, msglen, 1, fp);
    if (ret != 1) {
        strcpy(err, "error writing file");
        fclose(fp);
        goto error;
    }
    
    if (fclose(fp) != 0) {
        strcpy(err, "error completing file");
        goto error;
    }
    return 0;
    
error:
    time(&now);
    sprintf(errText, "%ld %hd %s %s: %s", (long)now, (short)ERR_WRITE, err, outFile, strerror(errno));
    return -1;
}





/* Parses a pick line and saves data into struct
 * 
 * Eg:
 * T Mod Inst Nmbr   S   C  N  L  DQ YYYYMMDDHHMMSS mS  Max val
 * 8 151 186 876284 NIE.HHN.VV.LM ?1 20221103165615.040 11 0 0
 * 
 * Returns zero if could not parse data
 */
int parsePick(char *line, PickLine *pick)
{
    const char pickSeparator[] = " ";
    const char scnlSeparator[] = ".";
    const char tagUP[] = "UP_";
    const char tagDOWN[] = "DOWN_";
    const char tagUNK[] = "UNKNOWN_";
    
    char *endField, *nextField;
    
    char scnl[32];
    char *endFieldSCNL, *nextFieldSCNL;
    
    int retval = 0;
        
    /* Error check */
    if (pick == NULL || line == NULL)
        return retval;
    
    /* Clear strings */
    pick->s[0] = '\0';
    pick->c[0] = '\0';
    pick->n[0] = '\0';
    pick->l[0] = '\0';
    pick->dateStr[0] = '\0';
    pick->timeStr[0] = '\0';
    pick->tag[0] = '\0';
    
    
    /* Position 0: TYPE_PICK_SCNL */
    nextField = strtok_r(line, pickSeparator, &endField);
    
    /* Position 1: ModuleID */
    nextField = strtok_r(NULL, pickSeparator, &endField);
    
    /* Position 2: InstID */
    nextField = strtok_r(NULL, pickSeparator, &endField);
    
    /* Position 3: PickID */
    nextField = strtok_r(NULL, pickSeparator, &endField);    
    
    
    
    
    /* Position 4: S.C.N.L */
    nextField = strtok_r(NULL, pickSeparator, &endField);
    if (nextField == NULL)
        return retval;
    
    /* Copy SCNL to new string to parse it */
    strncpy(scnl, nextField, sizeof(scnl));
    
    /* Position 4.1: S */
    nextFieldSCNL = strtok_r(scnl, scnlSeparator, &endFieldSCNL);
    if (nextFieldSCNL == NULL)
        return retval;
    
    strncpy(pick->s, nextFieldSCNL, sizeof(pick->s));
    if (strlen(pick->s) < 3) /* Check station name is long enough */
        return retval;
    else
        retval++; /* Valid parameter, increment counter */
    
    /* Position 4.2: C */
    nextFieldSCNL = strtok_r(NULL, scnlSeparator, &endFieldSCNL);
    if (nextFieldSCNL == NULL)
        return retval;
    
    strncpy(pick->c, nextFieldSCNL, sizeof(pick->c));
    if (strlen(pick->c) < 3) /* Check channel is long enough */
        return retval;
    else
        retval++; /* Valid parameter, increment counter */
   
    /* Position 4.3: N */
    nextFieldSCNL = strtok_r(NULL, scnlSeparator, &endFieldSCNL);
    if (nextFieldSCNL == NULL)
        return retval;
    
    strncpy(pick->n, nextFieldSCNL, sizeof(pick->n));
    retval++;
        
    /* Position 4.4: L */
    nextFieldSCNL = strtok_r(NULL, scnlSeparator, &endFieldSCNL);
    if (nextFieldSCNL == NULL)
        return retval;
    
    strncpy(pick->l, nextFieldSCNL, sizeof(pick->l));
    retval++;
    
    
    
    
    /* Position 5: Direction and quality */
    nextField = strtok_r(NULL, pickSeparator, &endField);
    if (nextField == NULL)
        return retval;
    
    switch (nextField[0]) {
        case 'U': strcpy(pick->tag, tagUP); break;
        case 'D': strcpy(pick->tag, tagDOWN); break;
        case '?': 
        default: strcpy(pick->tag, tagUNK);
    }
    /* Append quality number */
    strncat(pick->tag, nextField+1, 1);
    retval++;
    
    
        
    /* Position 6: DateTime */
    nextField = strtok_r(NULL, pickSeparator, &endField);
    if (nextField == NULL)
        return retval;
    
    if (strlen(nextField) < 14) /* Check datetime is long enough */
        return retval;
        
    if (snprintf(pick->dateStr, sizeof(pick->dateStr), "%.4s-%.2s-%.2s", nextField, nextField+4, nextField+6))
        retval++; /* Valid parameter, increment counter */
    if (snprintf(pick->timeStr, sizeof(pick->timeStr), "%.2s:%.2s:%.2s", nextField+8, nextField+10, nextField+12))
        retval++; /* Valid parameter, increment counter */
    
    return retval;
}



/* Build pick string according to Swarm
 * tag format:
 * 
 * YYYY-mm-dd HH:mm:ss ,  SSSS CCC NN LL ,  tag
 */
int buildPickString(PickLine pick, char *buffer)
{
    int bytesWritten = 0;
    
    /* Write first field */
    bytesWritten += sprintf(buffer + bytesWritten, "%s %s", pick.dateStr, pick.timeStr);
    
    /* Write separator */
    bytesWritten += sprintf(buffer + bytesWritten, "%s", CSVseparator);
    
    /* Write second field */
    bytesWritten += sprintf(buffer + bytesWritten, "%s %s %s %s", pick.s, pick.c, pick.n, pick.l);
    
    /* Write separator */
    bytesWritten += sprintf(buffer + bytesWritten, "%s", CSVseparator);
    
    /* Write third field */
    bytesWritten += sprintf(buffer + bytesWritten, "%s\n", pick.tag);
    
    return bytesWritten;
}

